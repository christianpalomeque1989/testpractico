﻿using Microsoft.EntityFrameworkCore;
using TestPractico.Models.Entities;

namespace TestPractico.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<SongSet> SongSets { get; set; }
        public DbSet<AlbumSet> AlbumSets { get; set; }
        public DbSet<PurchaseDetail> PurchaseDetails { get; set; }
        public DbSet<Client> Clients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Client>()
                .HasIndex(c => c.Id)
                .IsUnique();
        }
    }
}
