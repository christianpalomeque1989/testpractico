﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestPractico.Models.Entities
{
    public class AlbumSet
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "the {0} is required")]
        [DisplayName("Album Name")]
        public string Name { get; set; }

        public ICollection<SongSet> SongSets { get; set; }

        public ICollection<PurchaseDetail> PurchaseDetails { get; set; }
    }
}
