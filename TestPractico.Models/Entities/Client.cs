﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestPractico.Models.Entities
{
    public class Client
    {
        [Key]
        [MaxLength(10,ErrorMessage = "the field exceeds the maximum")]
        [DisplayName("Document")]
        [Required]
        public string Id { get; set; }

        [Required(ErrorMessage = "the {0} is required")]
        [MaxLength(100, ErrorMessage = "the field exceeds the maximum")]
        [DisplayName("Client Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "the {0} is required")]
        [MaxLength(50, ErrorMessage = "the field exceeds the maximum")]
        [EmailAddress]
        public string Mail { get; set; }


        [Required(ErrorMessage = "the {0} is required")]
        [MaxLength(500, ErrorMessage = "the field exceeds the maximum")]        
        public string Direction { get; set; }


        [Required(ErrorMessage = "the {0} is required")]
        [MaxLength(20, ErrorMessage = "the field exceeds the maximum")]        
        public string Phone { get; set; }

        public ICollection<PurchaseDetail> PurchaseDetails { get; set; }
    }
}
