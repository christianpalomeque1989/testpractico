﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestPractico.Models.Entities
{
    public class SongSet
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("Song Name")]
        [Required(ErrorMessage = "the {0} is required")]

        public string Name { get; set; }

        [Required]
        [ForeignKey(name: "Id")]
        [DisplayName("Album Name")]
        public int AlbumSetId { get; set; }

        [DisplayName("Album Name")]
        public virtual AlbumSet AlbumSet { get; set; }
    }
}
