﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TestPractico.Models.Entities
{
    public class PurchaseDetail
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "the {0} is required")]
        public double Total { get; set; }

        [Required]
        [ForeignKey(name: "Id")]
        [DisplayName("Album Name")]
        public int AlbumSetId { get; set; }

        public virtual AlbumSet AlbumSet { get; set; }

        [Required]
        [ForeignKey(name: "Id")]
        public string ClientId { get; set; }

        public virtual Client Client { get; set; }
    }
}
