﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestPractico.Data;
using TestPractico.Models.Entities;

namespace TestPractico.Web.Controllers
{
    public class SongSetsController : Controller
    {
        private readonly DataContext _context;

        public SongSetsController(DataContext context)
        {
            _context = context;
        }

        // GET: SongSets
        public async Task<IActionResult> Index()
        {
            var dataContext = _context.SongSets.Include(s => s.AlbumSet);
            return View(await dataContext.ToListAsync());
        }

        // GET: SongSets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var songSet = await _context.SongSets
                .Include(s => s.AlbumSet)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (songSet == null)
            {
                return NotFound();
            }

            return View(songSet);
        }

        // GET: SongSets/Create
        public IActionResult Create()
        {
            ViewData["AlbumSetId"] = new SelectList(_context.AlbumSets, "Id", "Name");            
            return View();
        }

        // POST: SongSets/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SongSet songSet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(songSet);
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            ViewData["AlbumSetId"] = new SelectList(_context.AlbumSets, "Id", "Name", songSet.AlbumSetId);
            return View(songSet);
        }

        // GET: SongSets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var songSet = await _context.SongSets.FindAsync(id);
            if (songSet == null)
            {
                return NotFound();
            }
            ViewData["AlbumSetId"] = new SelectList(_context.AlbumSets, "Id", "Name", songSet.AlbumSetId);
            return View(songSet);
        }

        // POST: SongSets/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, SongSet songSet)
        {
            if (id != songSet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _context.Update(songSet);
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            ViewData["AlbumSetId"] = new SelectList(_context.AlbumSets, "Id", "Name", songSet.AlbumSetId);
            return View(songSet);
        }

        // GET: SongSets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var songSet = await _context.SongSets
                .Include(s => s.AlbumSet)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (songSet == null)
            {
                return NotFound();
            }

            return View(songSet);
        }

        // POST: SongSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var songSet = await _context.SongSets.FindAsync(id);
            _context.SongSets.Remove(songSet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SongSetExists(int id)
        {
            return _context.SongSets.Any(e => e.Id == id);
        }
    }
}
