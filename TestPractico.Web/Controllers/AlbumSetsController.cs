﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TestPractico.Data;
using TestPractico.Models.Entities;

namespace TestPractico.Web.Controllers
{
    public class AlbumSetsController : Controller
    {
        private readonly DataContext _context;

        public AlbumSetsController(DataContext context)
        {
            _context = context;
        }

        // GET: AlbumSets
        public async Task<IActionResult> Index()
        {
            return View(await _context.AlbumSets.ToListAsync());
        }

        // GET: AlbumSets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AlbumSet albumSet = await _context.AlbumSets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (albumSet == null)
            {
                return NotFound();
            }

            return View(albumSet);
        }

        // GET: AlbumSets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AlbumSets/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AlbumSet albumSet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(albumSet);

                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (System.Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return View(albumSet);
        }

        // GET: AlbumSets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AlbumSet albumSet = await _context.AlbumSets.FindAsync(id);
            if (albumSet == null)
            {
                return NotFound();
            }
            return View(albumSet);
        }

        // POST: AlbumSets/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, AlbumSet albumSet)
        {
            if (id != albumSet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _context.Update(albumSet);
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (System.Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return View(albumSet);
        }

        // GET: AlbumSets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AlbumSet albumSet = await _context.AlbumSets
                .FirstOrDefaultAsync(m => m.Id == id);
            if (albumSet == null)
            {
                return NotFound();
            }

            return View(albumSet);
        }

        // POST: AlbumSets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            AlbumSet albumSet = await _context.AlbumSets.FindAsync(id);
            _context.AlbumSets.Remove(albumSet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AlbumSetExists(int id)
        {
            return _context.AlbumSets.Any(e => e.Id == id);
        }
    }
}
